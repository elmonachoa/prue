import requests, contextlib,json,random,hashlib,pandas
from time import time

def getregion():
    url = "https://rapidapi.p.rapidapi.com/all"

    headers = {
        'x-rapidapi-host': "restcountries-v1.p.rapidapi.com",
        'x-rapidapi-key': "0d16c6b6d5mshdf46fe35deb662bp137fb4jsne7fef79a4d01"
        }

    response = requests.get( url, headers=headers)
    reg=response.text
    res = json.loads(reg) 

    regiones=[]
    for i in range(len(res)):
        cont=0
        for j in range(len(regiones)):
            if(res[i]['region']==regiones[j]):
                cont=cont+1
        if(cont==0):
            regiones.append(res[i]['region'])
    return regiones

def getpais(reg):
    url = "https://restcountries.eu/rest/v2/all"
    response = requests.get(url)
    countries=response.text
    res = json.loads(countries)
    paises=[None for _ in range(len(reg))]
    posis=[None for _ in range(len(reg))]
    cont=0
    while(cont<7):
        ram=random.randint(0, len(res)-1)
        regi=res[ram]['region']
        for i in range(len(reg)):
            if (reg[i]==regi and paises[i]==None):
                paises[i]=res[ram]['name']
                posis[i]=ram
                cont=cont+1
    return paises, posis
def getidioma(posis):
    url = "https://restcountries.eu/rest/v2/all"
    response = requests.get(url)
    countries=response.text
    res = json.loads(countries)
    idiomas=[]
    for i in range (len(posis)):
        idiomas.append(res[posis[i]]['languages'][0]['name'])
    return idiomas
def getidiomacod(idioma):
    idiom=[]
    for i in range(len(idioma)):
        idiom.append(hashlib.sha1(idioma[i].encode()).hexdigest())

    return idiom
def gettabla(reg,paises,idiomacod):
    tabla=pandas.DataFrame(columns=['Region', 'City Name', 'Languaje'])
    times=[]
    tiempos=[]
    for i in range(len(reg)):
        start_time = time()
        tabla.loc[i]=[ reg[i], paises[i], idiomacod[i]]
        times.append(float(str((time() - start_time)*1000)[0:4]))
        tiempos.append(float(str((time() - start_time)*1000)[0:4]))
    tabla['Time']=tiempos
    return tabla, times