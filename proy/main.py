from modelo import getters
from connexion import connexionsql
import json
reg=getters.getregion()
paises, posis=getters.getpais(reg)
idioma=getters.getidioma(posis)
idiomacod=getters.getidiomacod(idioma)
tabla, times=getters.gettabla(reg,paises,idiomacod)
total=tabla['Time'].sum()
maxi=tabla['Time'].max()
mini=tabla['Time'].min()
prom=tabla['Time'].mean()
print(tabla)
print('Tiempo total: '+str(total)+'\nTiempo Maximo: '+str(maxi)+'\nTiempo Minimo: '+
str(mini)+'\nTiempo Promedio: '+str(prom))
con = connexionsql.sql_connection()
connexionsql.sql_table(con)
for i in range(len(reg)):
    time=float(times[i])
    connexionsql.insertar(reg[i],paises[i],idiomacod[i],time,con)
query=connexionsql.select(con)
with open('data.json', 'w') as file:
    json.dump(query, file, indent=4)
